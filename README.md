# Destination Prediction

A very basic example of how a destination recommender can be implemented in the most basic poupularity based way.

The data we use has been kindly provided by the Call-a-Bike bike sharing scheme of Deutsche Bahn. The data can be downloaded [here](https://data.deutschebahn.com/dataset/data-call-a-bike/resource/0fcce4dd-7fc6-43f8-a59c-983a7945f8ba)
and should be placed in its zipped form in the directory data.

In the directory code you will find a commented Jupyter notebook containing some data cleaning, data quality checks, feature engineering, exploratory analysis and finally a very basic recommender.

Lastly, the directory figs contains some graphs produced by the Jupyter notebook above.